/*
  Negin Mashreghi
  April 2019
  negin_mashreghi@yahoo.com
*/

var expect = require('chai').expect;
var { Builder, By, Key, until } = require('selenium-webdriver');

// ‘describe’ function defines "Test suite" and accepts a callback function.
describe('Test the addnumber form', () => {
  
  // the staging server URL 
  const stagingURL = "http://68.183.57.185:3000/"
  // 
  const driver = new Builder()
    
    /*
      In bitbucket-pipelines.yml you can define services and use it in stpes.
      This services share a network adapter with your build container and all open their ports on localhost. This is why we will use ‘localhost’ in the URL.
      Also, The default port the selenium server uses to listen for requests is port 4444. This is why port 4444 was used in the URL.
      more information 'https://www.seleniumhq.org/docs/07_selenium_grid.jsp'
    */
    
    // Sets the URL of a server to use
    .usingServer("http://localhost:4444/wd/hub")
    // Setting capabilities when requesting a new session
    .withCapabilities({"browserName":"chrome"})
    // Creates a new WebDriver client based on this builder's current configuration. 
    .build();
  
  
  
  // ‘it’ function define "test case" and accepts a callback function and contains the testing code.
  it('should add two numbers and show the result', async () => {

      // navigate to the staging server URL 
      await driver.get(stagingURL);
      //  findElement() -> find an element on the page
      //  sendKeys() -> type a sequence on the DOM element
      await driver.findElement(By.name('num1')).sendKeys('5', Key.ENTER);
      await driver.findElement(By.name('num2')).sendKeys('10', Key.ENTER);
      // click() -> click on this elemen
      await driver.findElement(By.id('add')).click();
      // getText() -> Get the visible (i.e. not hidden by CSS) innerText of spesific element,
      const output = await driver.findElement(By.id('output')).getText();
      // test if the innerText element with the ID of "output" is equal to 15
      expect(output).to.equal('15');
   
  });
  
  it('Should receive "invalid input" message when enter invalid input', async () => {

    await driver.get(stagingURL);
    await driver.findElement(By.name('num1')).sendKeys('hj', Key.ENTER);
    await driver.findElement(By.name('num2')).sendKeys('hn', Key.ENTER);
    await driver.findElement(By.id('add')).click();
    const output = await driver.findElement(By.className('err')).getText();
    // test if the innerText element with the ID of "output" is equal to invalid input message
    expect(output).to.equal('invalid input');
   
  });

  

  // after running all the test case quit() -> quit the current session
  after(async () => driver.quit());
});


