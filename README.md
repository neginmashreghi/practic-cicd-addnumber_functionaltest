
# Description

In this Repository the test.js file contains functional test with two test case.

#### Tools, Frameworks and Libraries

- [Mocha](https://mochajs.org/) is a feature-rich JavaScript test framework running on Node.js.
- [ChaiJS](ttps://www.chaijs.com) is a BDD / TDD assertion library for node.
- [Selenium-Webdriver](https://www.npmjs.com/package/selenium-webdriver) is browser automation library. Most often used for testing web-applications


---

## Mocha Introduction 

**Mocha** is a JavaScript test framework that runs tests on Node. Mocha comes in the form of a Node package via npm, allowing you to use any library for assertions as a replacement to Node’s standard ‘assert’ function, such as **ChaiJS**.


### Install Mocha

```
npm install -g mocha
```
### Install Chai Assertion Module

```
npm install --save chai

```



### Test Suite and Test Case Structure
In Mocha, a test suite is defined by the **‘describe’** keyword which accepts a callback function. A test suite can contain child / inner test suites, which can contain their own child test suites, etc. A test case is denoted by the **‘it’** function, which accepts a callback function and contains the testing code.



### Running Mocha Test Suite and Test Cases
Mocha supports execution of tests in three ways: 

1. Run whole Test Suite file:
```
mocha /path/to/test_suite.js
```
2. Run a specific suite or test from a specific suite file.If a suite is selected then all the child suites and/or tests will be executed.
```
mocha -g “Test-2” /path/to/test_suite.js
```
3. Run a specific suite or test file by searching recursively in a directory tree.
```
mocha --recursive -g “Test-2” /directory/
```


---

# Selenium Introduction
Selenium is a library that controls a web browser and emulates the user’s behavior. More specifically, Selenium offers specific language library APIs called ‘bindings’ for the user. These ‘bindings’ act as a client in order to perform requests to intermediate components and acting as servers in order to finally control a Browser.

### Selenium Installation

```
npm install selenium-webdriver
```

### WebDriver Construction

In order to be able to use Selenium, we should build the appropriate ‘webdriver’ object which will then control our browser. Below, we can see how we use the “Builder” pattern to construct a webdriver object by chaining several functions.
More info about [Class Builder](https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_Builder.html)

```
 const driver = new Builder()
    // Sets the URL of a remote WebDriver server to use
    .usingServer("http://localhost:4444/wd/hub")
    // Setting capabilities when requesting a new session
    .withCapabilities({"browserName":"chrome"})
    // Creates a new WebDriver client based on this builder's current configuration. 
    .build();

```
**.usingServer( url )  → Builder **

Sets the URL of a remote WebDriver server to use. Once a remote URL has been specified, the builder direct all new clients to that server. If this method is never called, the Builder will attempt to create all clients locally.

**Note**: To achieve automate testing with bitbucket pipeline you need to create a Docker container that runs a functional test and uses another container with selenium/standalone-chrome:3.4.0 image. 
In order to link two containers and runs them at the same time, we can define selenium container as services in the pipeline.
These services shared a network adapter with your build container and all open their ports on localhost. Also, selenium/standalone-chrome server uses port 4444 to listen for requests. 
This is why our remote WebDriver server URL is 'http://localhost:4444/wd/hub'.



**.withCapabilities( capabilities )  → Builder **

Sets the desired capabilities when requesting a new session. This will overwrite any previously set capabilities.

**.build() → ThenableWebDriver**

Creates a new WebDriver client based on this builder's current configuration.


---

# MochaJS + Selenium WebDriver

Generally speaking, Selenium WebDriver can be integrated with MochaJS since it is used in any plain NodeJS script. However, since Mocha doesn’t know when an asynchronous function has finished before a done() is called or a promise is returned, we have to be very careful with handling.

#### Promise Based
Selenium commands are registered automatically, to assure webdriver commands are executed in the correct sequential order a promise should be returned.

```
describe('Test the addnumber form', () => {
  const driver = new Builder()
    .usingServer("http://localhost:4444/wd/hub")
    .withCapabilities({"browserName":"chrome"})
    .build();

  it('should add two numbers and show the result', async () => {

      await driver.get('http://68.183.57.185:3000/');
      await driver.findElement(By.name('num1')).sendKeys('5', Key.ENTER);
      await driver.findElement(By.name('num2')).sendKeys('10', Key.ENTER);
      await driver.findElement(By.id('add')).click();
      const output = await driver.findElement(By.id('output')).getText();
      expect(output).to.equal('15');
   
  });

  after(async () => driver.quit());
});
```
The following actions will be executed:

1. Browser page with URL of (http://68.183.57.185:3000/) is loaded
2. Text Field with name of ‘num1’ is located and filled it with number ‘5’
3. Text Field with name of ‘num2’ is located and filled with number ‘10’
4. Bottum with id 'add' is located and clicked
5. paragraph tag with id 'output'is located retrieved the content 
6. checked if the innerText element with the ID of "output" is equal to 15
7. WebDriver quits and browser window is closed. Browser process is terminated.


---


For more detail on [test node with selenium-webdriver and mocha](https://www.sitepoint.com/how-to-test-your-javascript-with-selenium-webdriver-and-mocha/)
















